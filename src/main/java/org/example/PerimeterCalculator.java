package org.example;


import org.example.model.Circle;
import org.example.model.Rectangle;
import org.example.model.Square;
import org.example.service.CircleService;
import org.example.service.RectangleService;
import org.example.service.SquareService;

public class PerimeterCalculator {

    private SquareService squareService;
    private CircleService circleService;
    private RectangleService rectangleService;

    public PerimeterCalculator() {
        this.squareService = new SquareService();
        this.circleService = new CircleService();
        this.rectangleService = new RectangleService();
    }

    public double calculatePerimeter(Rectangle rectangle) {
        if(rectangle.fieldsNotNull())
            return rectangleService.perimeter(rectangle.getLength(), rectangle.getHeight());
        else
            throw new RuntimeException("Trying to calculate with length or height as 0");
    }

    public double calculatePerimeter(Square square) {
        if(square.fieldsNotNull()) {
            return squareService.perimeter(square.getLength());
        }else {
            throw new RuntimeException("Trying to calculate with length as 0");
        }
    }

    public double calculatePerimeter(Circle circle) {
        if(circle.fieldsNotNull())
            return circleService.circumference(circle.getRadius());
        else
            throw new RuntimeException("Trying to calculate with radius as 0");
    }

    public void setSquareService(SquareService squareService) {
        this.squareService = squareService;
    }

    public void setCircleService(CircleService circleService) {
        this.circleService = circleService;
    }

    public void setRectangleService(RectangleService rectangleService) {
        this.rectangleService = rectangleService;
    }
}
