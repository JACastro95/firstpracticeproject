package org.example;

import org.example.model.Circle;
import org.example.model.Rectangle;
import org.example.model.Square;
import org.example.service.CircleService;
import org.example.service.RectangleService;
import org.example.service.SquareService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        CalculateArea areaCalculator = new CalculateArea();
        PerimeterCalculator perimeterCalculator = new PerimeterCalculator();
        System.out.println("Circle=================");
        System.out.println("Area: " + areaCalculator.calculateArea(new Circle(3.0)));
        System.out.println("Circumference: " + perimeterCalculator.calculatePerimeter(new Circle(3.0)));
        System.out.println("Rectangle==============");
        System.out.println("Area: " + areaCalculator.calculateArea(new Rectangle(3.0, 7.0)));
        System.out.println("Perimeter: " + perimeterCalculator.calculatePerimeter(new Rectangle(3.0, 7.0)));
        System.out.println("Square=================");
        System.out.println("Area: " + areaCalculator.calculateArea(new Square(4.0)));
        System.out.println("Perimeter: " + perimeterCalculator.calculatePerimeter(new Square(4.0)));

    }
}
