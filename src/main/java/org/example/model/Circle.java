package org.example.model;

public class Circle implements Shape{

    private double radius;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public boolean fieldsNotNull() {
        return (this.radius != 0.0d);
    }
}
