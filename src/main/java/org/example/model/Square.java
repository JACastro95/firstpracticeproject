package org.example.model;

public class Square implements Shape{

    private double length;

    public Square() {
    }

    public Square(double length) {
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public boolean fieldsNotNull() {
        return (this.length != 0.0d);
    }
}
