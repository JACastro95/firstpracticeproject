package org.example.model;

public class Rectangle implements Shape{
    private double length;
    private double height;

    public Rectangle() {
    }

    public Rectangle(double length, double height) {
        this.length = length;
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public boolean fieldsNotNull() {
        return ((this.length != 0.0d) && (this.height != 0.0d));
    }
}
