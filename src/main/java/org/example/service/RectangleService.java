package org.example.service;

import org.example.model.Rectangle;

public class RectangleService {

    public double area(double length, double height) {
        return length * height;
    }

    public boolean isSquared(double length, double height) {
        return (length == height);
    }

    public double perimeter(double length, double height) {
        return (length * 2) + (height * 2);
    }

}
