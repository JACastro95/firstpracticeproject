package org.example.service;

import org.example.model.Square;

public class SquareService {

    public double area(double length) {
        return length * length;
    }

    public double perimeter(double length) {
        return length * 4;
    }
}
