package org.example.service;

import org.example.model.Circle;

public class CircleService {

    public double area(double radius) {
        return Math.PI * radius * radius;
    }

    public double circumference(double radius) {
        return Math.PI * 2 * radius;
    }
}
