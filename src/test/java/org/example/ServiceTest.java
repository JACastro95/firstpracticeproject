package org.example;

import org.example.service.CircleService;
import org.example.service.RectangleService;
import org.example.service.SquareService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ServiceTest {
    RectangleService rectangleService;
    CircleService circleService;
    SquareService squareService;

    @Before
    public void setup() {
        squareService = new SquareService();
        circleService = new CircleService();
        rectangleService = new RectangleService();
    }

    @Test
    public void testServices() {
        //square service
        double squareArea = squareService.area(3.0);
        Assert.assertEquals(9.0, squareArea, 0.0);

        double squarePerimeter = squareService.perimeter(3.0);
        Assert.assertEquals(12.0, squarePerimeter, 0.0);

        //circle service
        double circleArea = circleService.area(3.0);
        Assert.assertEquals(28.27, circleArea, 0.1);

        double circlePerimeter = circleService.circumference(3.0);
        Assert.assertEquals(18.85, circlePerimeter, 0.1);

        //rectangle service
        double rectangleArea = rectangleService.area(3.0, 5.0);
        Assert.assertEquals(15.0, rectangleArea, 0.0);

        double rectanglePerimeter = rectangleService.perimeter(3.0, 5.0);
        Assert.assertEquals(16.0, rectanglePerimeter, 0.0);

        Assert.assertEquals(true, rectangleService.isSquared(4.0, 4.0));
        Assert.assertEquals(false, rectangleService.isSquared(4.0, 6.0));
    }


}
