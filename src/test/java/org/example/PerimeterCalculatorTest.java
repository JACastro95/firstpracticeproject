package org.example;

import org.example.model.Circle;
import org.example.model.Rectangle;
import org.example.model.Square;
import org.example.service.CircleService;
import org.example.service.RectangleService;
import org.example.service.SquareService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class PerimeterCalculatorTest {
    //used for mock testing
    RectangleService mockedRectangleService;
    CircleService mockedCircleService;
    SquareService mockedSquareService;

    PerimeterCalculator perimeterCalculator;

    @Before
    public void setup() {
        mockedSquareService = mock(SquareService.class);
        mockedCircleService = mock(CircleService.class);
        mockedRectangleService = mock(RectangleService.class);

        perimeterCalculator = new PerimeterCalculator();

        perimeterCalculator.setCircleService(mockedCircleService);
        perimeterCalculator.setRectangleService(mockedRectangleService);
        perimeterCalculator.setSquareService(mockedSquareService);

        when(mockedSquareService.perimeter(anyDouble())).thenReturn(5.0);
        when(mockedCircleService.circumference(anyDouble())).thenReturn(5.0);
        when(mockedRectangleService.perimeter(anyDouble(), anyDouble())).thenReturn(5.0);
    }

    @Test
    public void testAreaCalculations() {
        double squarePerimeter = perimeterCalculator.calculatePerimeter(new Square(4.0));
        Assert.assertEquals(5.0, squarePerimeter, 0.0);

        double circleCircumference = perimeterCalculator.calculatePerimeter(new Circle(3.0));
        Assert.assertEquals(5.0, circleCircumference, 0.0);

        double rectanglePerimeter = perimeterCalculator.calculatePerimeter(new Rectangle(3.0, 7.0));
        Assert.assertEquals(5.0, rectanglePerimeter, 0.5);

        //testing exceptions
        try {
            perimeterCalculator.calculatePerimeter(new Square());
        }catch (RuntimeException rex) {
            Assert.assertEquals("Trying to calculate with length as 0", rex.getMessage());
        }

        try {
            perimeterCalculator.calculatePerimeter(new Circle());
        }catch (RuntimeException rex) {
            Assert.assertEquals("Trying to calculate with radius as 0", rex.getMessage());
        }

        try {
            perimeterCalculator.calculatePerimeter(new Rectangle());
        }catch (RuntimeException rex) {
            Assert.assertEquals("Trying to calculate with length or height as 0", rex.getMessage());
        }

    }
}
