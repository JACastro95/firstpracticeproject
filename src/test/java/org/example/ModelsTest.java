package org.example;

import org.example.model.Circle;
import org.example.model.Rectangle;
import org.example.model.Square;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ModelsTest {

    Square square;
    Circle circle;
    Rectangle rectangle;

    @Before
    public void setup() {
        circle = new Circle(2.0);
        square = new Square(4.0);
        rectangle = new Rectangle(3.0, 5.0);
    }

    @Test
    public void testModels() {
        //circle
        Assert.assertEquals(2.0, circle.getRadius(), 0.0);
        Assert.assertEquals(true, circle.fieldsNotNull());
        circle.setRadius(3.0);
        Assert.assertEquals(3.0, circle.getRadius(), 0.0);

        //square
        Assert.assertEquals(4.0, square.getLength(), 0.0);
        Assert.assertEquals(true, square.fieldsNotNull());
        square.setLength(8.0);
        Assert.assertEquals(8.0, square.getLength(), 0.0);

        //rectangle
        Assert.assertEquals(3.0, rectangle.getLength(), 0.0);
        Assert.assertEquals(5.0, rectangle.getHeight(), 0.0);
        Assert.assertEquals(true, rectangle.fieldsNotNull()); //all true path
        rectangle.setLength(0.0);
        rectangle.setHeight(0.0);
        Assert.assertEquals(false, rectangle.fieldsNotNull()); //all false path
        rectangle.setLength(4.0);
        rectangle.setHeight(0.0);
        Assert.assertEquals(false, rectangle.fieldsNotNull()); //middle path
        rectangle.setLength(8.0);
        rectangle.setHeight(3.0);
        Assert.assertEquals(8.0, rectangle.getLength(), 0.0);
        Assert.assertEquals(3.0, rectangle.getHeight(), 0.0);
    }

}
