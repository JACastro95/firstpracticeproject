package org.example;

import org.example.model.Circle;
import org.example.model.Rectangle;
import org.example.model.Square;
import org.example.service.CircleService;
import org.example.service.RectangleService;
import org.example.service.SquareService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class CalculateAreaTest {
    //used for mock testing
    RectangleService mockedRectangleService;
    CircleService mockedCircleService;
    SquareService mockedSquareService;

    CalculateArea areaCalculator;

    @Before
    public void setup() {
        //setup mocks
        mockedSquareService = mock(SquareService.class);
        mockedCircleService = mock(CircleService.class);
        mockedRectangleService = mock(RectangleService.class);

        areaCalculator = new CalculateArea();

        areaCalculator.setCircleService(mockedCircleService);
        areaCalculator.setRectangleService(mockedRectangleService);
        areaCalculator.setSquareService(mockedSquareService);

        when(mockedSquareService.area(anyDouble())).thenReturn(5.0);
        when(mockedCircleService.area(anyDouble())).thenReturn(5.0);
        when(mockedRectangleService.area(anyDouble(), anyDouble())).thenReturn(5.0);
    }

    @Test
    public void testAreaCalculations() {
        double squareArea = areaCalculator.calculateArea(new Square(4.0));
        Assert.assertEquals(5.0, squareArea, 0.0);

        double circleArea = areaCalculator.calculateArea(new Circle(3.0));
        Assert.assertEquals(5.0, circleArea, 0.5);

        double rectangleArea = areaCalculator.calculateArea(new Rectangle(3.0, 7.0));
        Assert.assertEquals(5.0, rectangleArea, 0.5);

        //testing exceptions
        try {
            areaCalculator.calculateArea(new Square());
        }catch (RuntimeException rex) {
            Assert.assertEquals("Trying to calculate with length as 0", rex.getMessage());
        }

        try {
            areaCalculator.calculateArea(new Circle());
        }catch (RuntimeException rex) {
            Assert.assertEquals("Trying to calculate with radius as 0", rex.getMessage());
        }

        try {
            areaCalculator.calculateArea(new Rectangle());
        }catch (RuntimeException rex) {
            Assert.assertEquals("Trying to calculate with length or height as 0", rex.getMessage());
        }

    }


}
